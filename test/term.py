#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# Copyright © 2021 Lénaïc Bagnères, lenaicb@singularity.fr

# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import os
import sys

os.chdir(os.path.dirname(os.path.realpath(__file__)))
sys.path.append('../src')
import hnc.term

if __name__ == '__main__':

    print()
    print(hnc.term.green('-') * 80)
    print(hnc.term.green('Test hnc.term functions'))
    print(hnc.term.green('-') * 80)

    # --------------------------------------------------------------------------
    # VT100 Terminal Colors
    # --------------------------------------------------------------------------

    print()
    print(hnc.term.blue('Test color functions'))

    print()
    print(hnc.term.grey('Grey text'))
    print(hnc.term.red('Red text'))
    print(hnc.term.green('Green text'))
    print(hnc.term.orange('Orange text'))
    print(hnc.term.blue('Blue text'))
    print(hnc.term.purple('Purple text'))


    # --------------------------------------------------------------------------
    # End
    # --------------------------------------------------------------------------

    print()
    sys.exit(0)
