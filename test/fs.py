#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# Copyright © 2021 Lénaïc Bagnères, lenaicb@singularity.fr

# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import os
import sys

os.chdir(os.path.dirname(os.path.realpath(__file__)))
sys.path.append('../src')
import hnc.fs
import hnc.term

if __name__ == '__main__':

    r = 0

    # --------------------------------------------------------------------------
    # Start
    # --------------------------------------------------------------------------

    print()
    print(hnc.term.green('-') * 80)
    print(hnc.term.green('Test hnc.fs functions'))
    print(hnc.term.green('-') * 80)

    path_basename = 'hnc_fs_test_file'
    path_ext = 'txt'
    path = path_basename + '.' + path_ext

    # Create temporary tesxt file for the tests
    f = open(path, 'w')
    f.write('Temporary file to test hnc/fs.py')
    f.close()

    # --------------------------------------------------------------------------
    # Hash
    # --------------------------------------------------------------------------

    print()
    print(hnc.term.blue('Test hash functions'))

    # md5
    print()
    print('Compute md5 of', path)
    md5 = hnc.fs.md5(path)
    print(md5)
    # Test
    md5_ref = '7db42d8fe550f12b3df6a9cff90b43f1'
    if md5 != md5_ref:
        print(hnc.term.red('ERROR:'), 'md5 should be', md5_ref)
        r += 1

    # --------------------------------------------------------------------------
    # Symlink
    # --------------------------------------------------------------------------

    print()
    print(hnc.term.blue('Test symlink functions'))

    # Create symlink
    symlink_path = path_basename + '_symlink.' + path_ext
    print()
    print('Create symlink', symlink_path)
    hnc.fs.symlink(path, symlink_path)
    # Test 1
    if os.path.islink(symlink_path) == False:
        print(hnc.term.red('ERROR:'), symlink_path, 'is not a symlink')
        r += 1
    # Test 2
    md5 = hnc.fs.md5(symlink_path)
    md5_symlink = hnc.fs.md5(symlink_path)
    if os.path.islink(symlink_path) == False:
        print(hnc.term.red('ERROR:'), symlink_path, 'is not a symlink')
        r += 1
    # Remove symlink
    os.remove(symlink_path)

    # --------------------------------------------------------------------------
    # End
    # --------------------------------------------------------------------------

    print()

    # Delete temporary text file
    os.remove(path)

    sys.exit(r)
