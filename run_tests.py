#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# Copyright © 2021 Lénaïc Bagnères, lenaicb@singularity.fr

# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import os
import sys

sys.path.append('src')
import hnc.term

if __name__ == '__main__':

    nb_fails = 0

    test_dir = 'test'
    tests = sorted(os.listdir(test_dir))

    print(hnc.term.blue('-') * 80)
    print(hnc.term.blue(str(len(tests)) + ' TEST(S) TO BE EXECUTED:'))
    print(hnc.term.blue('-') * 80)
    for test in tests:
        print('-', test)
    print()

    tests_succeeded = [ ]

    tests_failed = [ ]

    for test in tests:
        test_path = os.path.join(test_dir, test)
        cmd = 'python3 ' + test_path
        r = os.system(cmd)
        if r == 0:
            tests_succeeded += [ test ]
        else:
            tests_failed += [ test ]

    print(hnc.term.green('-') * 80)
    print(hnc.term.green(str(len(tests_succeeded)) + ' TEST(S) SUCCEEDED:'))
    print(hnc.term.green('-') * 80)
    for test in tests_succeeded:
        print('-', test)
    print()

    print(hnc.term.red('-') * 80)
    print(hnc.term.red(str(len(tests_failed)) + ' TEST(S) FAILED:'))
    print(hnc.term.red('-') * 80)
    for test in tests_failed:
        print('-', test)
    print()

    sys.exit(len(tests_failed))
