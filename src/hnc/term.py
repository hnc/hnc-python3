#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# Copyright © 2021 Lénaïc Bagnères, lenaicb@singularity.fr

# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# ------------------------------------------------------------------------------
# VT100 Terminal Colors
# ------------------------------------------------------------------------------

def grey(s):
    '''Return the string s with grey color code for VT100 terminal'''
    return '\033[90m' + s +'\033[0m'

def red(s):
    '''Return the string s with grey color code for VT100 terminal'''
    return '\033[91m' + s +'\033[0m'

def green(s):
    '''Return the string s with grey color code for VT100 terminal'''
    return '\033[92m' + s +'\033[0m'

def orange(s):
    '''Return the string s with grey color code for VT100 terminal'''
    return '\033[93m' + s +'\033[0m'

def blue(s):
    '''Return the string s with grey color code for VT100 terminal'''
    return '\033[94m' + s +'\033[0m'

def purple(s):
    '''Return the string s with grey color code for VT100 terminal'''
    return '\033[95m' + s +'\033[0m'
