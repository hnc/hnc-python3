#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# Copyright © 2021 Lénaïc Bagnères, lenaicb@singularity.fr

# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import os
import hashlib

# ------------------------------------------------------------------------------
# Hash
# ------------------------------------------------------------------------------

def md5(path):
    '''Compute the md5 of file'''
    m = hashlib.md5()
    f = open(path, 'rb')
    while True:
        buf = f.read(2048)
        if not buf: break
        m.update(buf)
    f.close()
    return m.hexdigest()

# ------------------------------------------------------------------------------
# Symlink
# ------------------------------------------------------------------------------

def symlink(src, dst):
    '''
    Create a symlink: src -> dest

    If destination already exist, does nothing
    '''
    if os.name == 'posix':
        try: os.symlink(os.path.abspath(src), dst)
        except FileExistsError: pass
    else:
        raise Exception('hnc.fs.symlink is not implemented for your OS (' + os.name + ')')
